# API-Skeleton

This skeleton provides setup for JSON API.
It's based on Slim 4, Doctrine 2, PHP DI and JSON API library.

## Install
```bash
$ composer create-project bednic/api-skeleton <path> "0.1.*"
```

### Own DI configuration file
If you want own DI configuration or overload current,
then you just add file like `di-config.local.php` to /config folder.
It will rewrite default DI configuration.

### Own ENV
You can override environment by own `.env` file. Just make own file, e.g. `.env.local.`

## Structure
```
├───/app
│   ├───/Controller
│   ├───/Factory
│   ├───/Migration
│   ├───/Model
│   │   ├───/Entity
│   │   └───/Repository
│   └───/Service
│
├───/cache
├───/config
├───/log
├───/public
├───/tmp
└───/vendor
```

## Docker
You can start developing via Docker
```bash
docker build -t . && docker run -p 80:80 -v /path/to/project:/var/www --name api
```

## Docker Compose
```bash
docker-composer up -d
```

> There is a couple of thinks you have to do, if you want skeleton works from start.
```
docker exec -it api vendor/bin/doctrine orm:generate:proxies
docker exec -it api vendor/bin/doctrine-migrations migrate
```
`api` is the name of api container. The migration ask of confirmation, so say yes.

After these steps you should have working API, just hit http://localhost. You should see
generated OAS schema.

## Issues

* [Gitlab](https://gitlab.com/bednic/api-skeleton/issues)
* [Email](mailto:incoming+bednic-api-skeleton-12263057-issue-@incoming.gitlab.com)
