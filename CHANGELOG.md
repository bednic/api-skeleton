# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

### [0.1.1](https://gitlab.com/bednic/api-skeleton/compare/0.1.0...0.1.1) (2020-10-30)

## [0.1.0](https://gitlab.com/bednic/api-skeleton/compare/0.0.1...0.1.0) (2020-10-27)


### ⚠ BREAKING CHANGES

* Refactor whole project ([4fd5d2a](https://gitlab.com/bednic/api-skeleton/commit/4fd5d2aa454a06698a5079a733c25e0d78af2658))

### Added

* ConnectionFactory ([fad3c44](https://gitlab.com/bednic/api-skeleton/commit/fad3c443579ff7d29eedbbe99d1d7533ef60d2cf))


### Changed

* move Entity to folder Entity ([873ba60](https://gitlab.com/bednic/api-skeleton/commit/873ba603c9385771ae438ff03804da5c901220e4))
* Several improvements ([2588444](https://gitlab.com/bednic/api-skeleton/commit/2588444056f8d9b6ffd652b7785bd5ca49a850a5))


### Fixed

* throw exception when relationship entity not found ([1e5b5b5](https://gitlab.com/bednic/api-skeleton/commit/1e5b5b59a1bcf212752fa4c131b71dfbd5014a6e))
