FROM bednic/php-runtime:7.4

# DIRECTORIES SETUP
RUN mkdir -p public log tmp cache \
&& chown -R www-data:www-data public log tmp cache \
&& chmod 770 tmp log cache

COPY . .

RUN composer install
