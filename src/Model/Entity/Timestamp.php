<?php

declare(strict_types=1);

namespace API\Model\Entity;

use DateTime;
use Exception;

/**
 * Trait Timestamp
 * @package DW\API\Model\Entity
 */
trait Timestamp
{
    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=false, options={"default":"CURRENT_TIMESTAMP"})
     */
    protected DateTime $created;

    /**
     * @var DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected ?DateTime $updated = null;

    /**
     * @return DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdated(): ?DateTime
    {
        return $this->updated;
    }

    /**
     * Method called before update entity
     *
     * @ORM\PreUpdate
     * @throws Exception
     */
    public function onUpdate(): void
    {
        $this->updated = new DateTime();
    }

    /**
     * Method called before persist entity
     *
     * @ORM\PrePersist
     * @throws Exception
     */
    public function onCreate(): void
    {
        $this->created = new DateTime();
    }
}
