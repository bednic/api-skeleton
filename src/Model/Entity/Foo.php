<?php

declare(strict_types=1);

namespace API\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JSONAPI\Annotation as API;

/**
 * Class Foo
 *
 * @package API\Model\Entity
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @API\Resource
 */
class Foo extends Entity
{
    use Timestamp;

    /**
     * @var int
     * @ORM\Column
     */
    private int $number = 0;
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity=Bar::class, mappedBy="foo", cascade={"ALL"})
     */
    private Collection $bars;

    public function __construct()
    {
        $this->bars = new ArrayCollection();
    }

    /**
     * @API\Attribute
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber(int $number): void
    {
        $this->number = $number;
    }

    /**
     * @API\Relationship(target=Bar::class)
     * @return Collection
     */
    public function getBars(): Collection
    {
        return $this->bars;
    }

    /**
     * @param Collection $bars
     */
    public function setBars(Collection $bars): void
    {
        /** @var Bar $bar */
        foreach ($this->bars as $bar) {
            $bar->setFoo(null);
        }
        foreach ($bars as $bar) {
            $bar->setFoo($this);
        }
        $this->bars = $bars;
    }
}
