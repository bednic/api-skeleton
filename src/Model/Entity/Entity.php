<?php

/**
 * Created by IntelliJ IDEA.
 * User: tomas
 * Date: 15.04.2019
 * Time: 14:15
 */

namespace API\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use JSONAPI\Annotation as API;

/**
 * Class Entity
 * Basic class from which every entity should inherit. Still this is just example.
 * Don't forget to add HasLifecycleCallbacks annotation on child entity to invoke callbacks like ::onUpdate,
 * ::onCreate, etc...
 *
 * @package API\Model\JSprit\Entity
 * @ORM\MappedSuperclass
 */
abstract class Entity
{
    /**
     * @var int|null
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected ?int $id = null;

    /**
     * @return int
     * @API\Id
     */
    public function getId(): int
    {
        return $this->id;
    }
}
