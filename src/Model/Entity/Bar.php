<?php

declare(strict_types=1);

namespace API\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use JSONAPI\Annotation as API;

/**
 * Class Bar
 *
 * @package API\Model\Entity
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @API\Resource
 */
class Bar extends Entity
{
    use Timestamp;

    /**
     * @var Foo|null
     * @ORM\ManyToOne(targetEntity=Foo::class, inversedBy="bars", cascade={"ALL"})
     */
    private ?Foo $foo  = null;
    /**
     * @var string
     * @ORM\Column
     */
    private string $value;

    /**
     * @return Foo|null
     * @API\Relationship(target=Foo::class)
     */
    public function getFoo(): ?Foo
    {
        return $this->foo;
    }

    /**
     * @param Foo|null $foo
     */
    public function setFoo(?Foo $foo): void
    {
        $this->foo = $foo;
    }

    /**
     * @return string
     * @API\Attribute
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }
}
