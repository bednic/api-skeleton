<?php


namespace API\Model\Repository;


use Doctrine\ORM\EntityRepository;

/**
 * Class DefaultRepository
 * This class serves as wrapper for Doctrine EntityRepository, feel free to overwrite common methods
 *
 * @package App\Model\Repository
 */
class DefaultRepository extends EntityRepository
{

}
