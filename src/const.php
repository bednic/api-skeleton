<?php

define('ROOT', realpath(__DIR__ . '/../'));
define('LOG', ROOT . '/log');
define('CACHE', ROOT . '/cache');
define('TMP', ROOT . '/tmp');
define('PUB', ROOT . '/public');
