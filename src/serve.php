<?php

declare(strict_types=1);

use API\Factory\ResourceRouteRegistrar;
use API\Middleware\CORSMiddleware;
use JSONAPI\Metadata\MetadataRepository;
use JSONAPI\Middleware\PsrJsonApiMiddleware;
use JSONAPI\OAS\Factory\OpenAPISpecificationBuilder;
use JSONAPI\OAS\Info;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Http\Response;

require_once __DIR__ . '/../src/const.php';
/**
 * @var $container ContainerInterface
 */
$container = include_once __DIR__ . '/../src/bootstrap.php';

// APP
/** @var App $app */
$app = $container->get(App::class);
// MIDDLEWARE (LIFO)
$app->addBodyParsingMiddleware();
$app->addMiddleware($container->get(CORSMiddleware::class));
$app->addRoutingMiddleware();
$app->addErrorMiddleware(
    $container->get('displayErrorDetails'),
    $container->get('logErrors'),
    $container->get('logErrorDetails'),
    $container->get(LoggerInterface::class)
);
$app->group('', [$container->get(ResourceRouteRegistrar::class), 'register'])
    ->addMiddleware($container->get(PsrJsonApiMiddleware::class));
$app->get('/', function (ServerRequestInterface $request, Response $response) use ($container): ResponseInterface {
    $factory = new OpenAPISpecificationBuilder(
        $container->get(MetadataRepository::class),
        $container->get('baseURL')
    );
    $info    = new Info('API Schema', '0.1.0');
    $oas     = $factory->create($info);
    return $response->withJson($oas);
});
$app->options(
    '/[{path:.*}]',
    function (ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $response;
    }
);
$app->run();
