<?php

use DI\ContainerBuilder;
use Symfony\Component\Dotenv\Dotenv;

require __DIR__ . "/../vendor/autoload.php";

// DIRECTORIES
!file_exists(LOG) ? mkdir(LOG, 0770) : null;
!file_exists(CACHE) ? mkdir(CACHE, 0770) : null;
!file_exists(TMP) ? mkdir(TMP, 0770) : null;

// ENV
if (file_exists(__DIR__ . "/../.env")) {
    (new Dotenv())->loadEnv(__DIR__ . "/../.env");
}
$env = getenv("API_ENV") ? getenv("API_ENV") : "dev";

// Create DI
$containerBuilder = new ContainerBuilder();
$containerBuilder->addDefinitions(__DIR__ . "/../config/di-config.php");

// Override DI by own DI config, e.g. for local development make di-config.local.php in /config/ directory
$iter = new DirectoryIterator(__DIR__ . "/../config");
foreach ($iter as $fileInfo) {
    if (
        !$fileInfo->isDot()
        && (preg_match('/^di-config\..+\.php$/', $fileInfo->getFilename()))
    ) {
        $containerBuilder->addDefinitions($fileInfo->getRealPath());
    }
}

if ($env === "prod") {
    /*
     * Remember when you using cache in production
     * it's necessary clear caches after each deploy to regenerate cache
     */
    $containerBuilder->enableCompilation(CACHE . DIRECTORY_SEPARATOR . "_DI");
    $containerBuilder->writeProxiesToFile(true, CACHE . DIRECTORY_SEPARATOR . "_DI.Proxy");
    // Remember! This require APCu cache enabled
    $containerBuilder->enableDefinitionCache();
}

return $containerBuilder->build();
