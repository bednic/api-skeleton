<?php

namespace API\Service\Logging;

use API\Service\IService;
use Doctrine\DBAL\Logging\SQLLogger;
use Psr\Log\LoggerInterface;

class CompositeSQLLogger implements SQLLogger, IService
{
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     *  constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


    /**
     * Logs a SQL statement somewhere.
     *
     * @param string              $sql    The SQL to be executed.
     * @param mixed[]|null        $params The SQL parameters.
     * @param int[]|string[]|null $types  The SQL parameter types.
     *
     * @return void
     */
    public function startQuery($sql, ?array $params = null, ?array $types = null)
    {
        $msg = $sql . ' [' . print_r($params, true) . '] [' . print_r($types, true) . ']';
        $this->logger->debug($msg);
    }

    /**
     * Marks the last started query as stopped. This can be used for timing of queries.
     *
     * @return void
     */
    public function stopQuery()
    {
    }
}
