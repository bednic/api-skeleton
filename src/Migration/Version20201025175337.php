<?php

declare(strict_types=1);

namespace API\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201025175337 extends AbstractMigration {
    public function getDescription(): string
    {
        return 'FooBar';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE Bar (id INT AUTO_INCREMENT NOT NULL, foo_id INT DEFAULT NULL, value VARCHAR(255) NOT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated DATETIME DEFAULT NULL, INDEX IDX_4EB2CA4A8E48560F (foo_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Foo (id INT AUTO_INCREMENT NOT NULL, number VARCHAR(255) NOT NULL, created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Bar ADD CONSTRAINT FK_4EB2CA4A8E48560F FOREIGN KEY (foo_id) REFERENCES Foo (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE Bar DROP FOREIGN KEY FK_4EB2CA4A8E48560F');
        $this->addSql('DROP TABLE Bar');
        $this->addSql('DROP TABLE Foo');
    }
}
