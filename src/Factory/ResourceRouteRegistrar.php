<?php

declare(strict_types=1);

namespace API\Factory;

use API\Controller\ResourceController;
use Fig\Http\Message\RequestMethodInterface;
use JSONAPI\Annotation\Relationship;
use JSONAPI\Metadata\MetadataRepository;
use JSONAPI\Middleware\PsrJsonApiMiddleware;
use JSONAPI\OAS\Factory\OpenAPISpecificationBuilder;
use JSONAPI\OAS\Info;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Http\Response;
use Slim\Routing\RouteCollectorProxy;

/**
 * Class RouteFactory
 *
 * @package DW\API\Factory
 */
class ResourceRouteRegistrar implements IFactory
{
    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;
    /**
     * @var string
     */
    protected string $baseURL;
    /**
     * @var App
     */
    protected App $app;
    /**
     * @var MetadataRepository
     */
    protected MetadataRepository $repository;
    /**
     * @var ResourceController
     */
    protected ResourceController $controller;
    /**
     * @var PsrJsonApiMiddleware
     */
    private PsrJsonApiMiddleware $middleware;

    /**
     * RouteFactory constructor.
     *
     * @param string               $baseURL
     * @param App                  $app
     * @param MetadataRepository   $repository
     * @param ResourceController   $controller
     * @param PsrJsonApiMiddleware $middleware
     * @param LoggerInterface      $logger
     */
    public function __construct(
        string $baseURL,
        App $app,
        MetadataRepository $repository,
        ResourceController $controller,
        PsrJsonApiMiddleware $middleware,
        LoggerInterface $logger
    ) {
        $this->baseURL = $baseURL;
        $this->app     = $app;
        $this->repository = $repository;
        $this->controller = $controller;
        $this->logger     = $logger;
        $this->middleware = $middleware;
    }

    /**
     * @param RouteCollectorProxy $group
     */
    public function register(RouteCollectorProxy $group)
    {
        foreach ($this->repository->getAll() as $className => $classMetadata) {
            $resource   = $classMetadata->getType();
            $collection = "/" . $resource;
            $single     = $collection . "/{id}";
            $group->get($collection, [$this->controller, ResourceController::GET_COLLECTION]);
            $group->get($single, [$this->controller, ResourceController::GET_RESOURCE]);
            if (!$classMetadata->isReadOnly()) {
                $group->post($collection, [$this->controller, ResourceController::POST_RESOURCE]);
                $group->patch($single, [$this->controller, ResourceController::PATCH_RESOURCE]);
                $group->delete($single, [$this->controller, ResourceController::DELETE_RESOURCE]);
            }
            $toMany = [];
            $toOne  = [];
            /**
             * @var string       $key
             * @var Relationship $relationship
             */
            foreach ($classMetadata->getRelationships() as $key => $relationship) {
                if ($relationship->isCollection) {
                    $toMany[] = $key;
                } else {
                    $toOne[] = $key;
                }
            }
            if ($toMany) {
                $relationships = $single . "/relationships/{name:" . (implode("|", $toMany)) . "}";
                $relations     = $single . "/{name:" . (implode("|", $toMany)) . "}";
                $group->post($relationships, [$this->controller, ResourceController::POST_RELATIONSHIPS]);
                $group->patch($relationships, [$this->controller, ResourceController::PATCH_RELATIONSHIPS]);
                $group->delete($relationships, [$this->controller, ResourceController::DELETE_RELATIONSHIPS]);
                $group->get($relationships, [$this->controller, ResourceController::GET_RELATIONSHIPS]);
                $group->get($relations, [$this->controller, ResourceController::GET_RELATIONSHIPS]);
            }
            if ($toOne) {
                $relationships = $single . "/relationships/{name:" . (implode("|", $toOne)) . "}";
                $relations     = $single . "/{name:" . (implode("|", $toOne)) . "}";
                $group->patch($relationships, [$this->controller, ResourceController::PATCH_RELATIONSHIPS]);
                $group->get($relationships, [$this->controller, ResourceController::GET_RELATIONSHIPS]);
                $group->get($relations, [$this->controller, ResourceController::GET_RELATIONSHIPS]);
            }
        }
    }
}
