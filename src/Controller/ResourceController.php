<?php

namespace API\Controller;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Response;

/**
 * Interface ResourceController
 *
 * @package API\Controller
 */
interface ResourceController
{
    public const GET_COLLECTION = 'getCollection';
    public const GET_RESOURCE = 'getResource';
    public const POST_RESOURCE = 'createResource';
    public const PATCH_RESOURCE = 'updateResource';
    public const DELETE_RESOURCE = 'removeResource';
    public const GET_RELATIONSHIPS = 'getRelationships';
    public const PATCH_RELATIONSHIPS = 'updateRelationships';
    public const POST_RELATIONSHIPS = 'createRelationships';
    public const DELETE_RELATIONSHIPS = 'removeRelationships';

    /**
     * @param ServerRequestInterface $request
     * @param Response               $response
     *
     * @return ResponseInterface
     */
    public function getCollection(ServerRequestInterface $request, Response $response): ResponseInterface;

    /**
     * @param ServerRequestInterface $request
     * @param Response               $response
     * @param array                  $args
     *
     * @return ResponseInterface
     */
    public function getResource(ServerRequestInterface $request, Response $response, array $args): ResponseInterface;

    /**
     * @param ServerRequestInterface $request
     * @param Response               $response
     *
     * @return ResponseInterface
     */
    public function createResource(ServerRequestInterface $request, Response $response): ResponseInterface;

    /**
     * @param ServerRequestInterface $request
     * @param Response               $response
     *
     * @return ResponseInterface
     */
    public function updateResource(ServerRequestInterface $request, Response $response): ResponseInterface;

    /**
     * @param ServerRequestInterface $request
     * @param Response               $response
     * @param array                  $args
     *
     * @return ResponseInterface
     */
    public function removeResource(ServerRequestInterface $request, Response $response, array $args): ResponseInterface;

    /**
     * @param ServerRequestInterface $request
     * @param Response               $response
     * @param array                  $args
     *
     * @return ResponseInterface
     */
    public function getRelationships(
        ServerRequestInterface $request,
        Response $response,
        array $args
    ): ResponseInterface;

    /**
     * @param ServerRequestInterface $request
     * @param Response               $response
     * @param array                  $args
     *
     * @return ResponseInterface
     */
    public function updateRelationships(
        ServerRequestInterface $request,
        Response $response,
        array $args
    ): ResponseInterface;

    /**
     * Only for toMany
     *
     * @param ServerRequestInterface $request
     * @param Response               $response
     * @param array                  $args
     *
     * @return ResponseInterface
     */
    public function createRelationships(
        ServerRequestInterface $request,
        Response $response,
        array $args
    ): ResponseInterface;

    /**
     * Only for toMany
     *
     * @param ServerRequestInterface $request
     * @param Response               $response
     * @param array                  $args
     *
     * @return ResponseInterface
     */
    public function removeRelationships(
        ServerRequestInterface $request,
        Response $response,
        array $args
    ): ResponseInterface;
}
