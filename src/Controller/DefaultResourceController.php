<?php

namespace API\Controller;

use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\Collection;
use API\Model\Entity\Entity;
use API\Model\Repository\DefaultRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\TransactionRequiredException;
use Exception;
use Fig\Http\Message\StatusCodeInterface;
use JSONAPI\Document\Document;
use JSONAPI\Document\PrimaryData;
use JSONAPI\Document\ResourceCollection;
use JSONAPI\Document\ResourceObject;
use JSONAPI\Document\ResourceObjectIdentifier;
use JSONAPI\DocumentBuilder;
use JSONAPI\DocumentBuilderFactory;
use JSONAPI\Exception\Document\DocumentException;
use JSONAPI\Exception\Document\FieldNotExist;
use JSONAPI\Exception\Document\ForbiddenCharacter;
use JSONAPI\Exception\Document\ForbiddenDataType;
use JSONAPI\Exception\Driver\DriverException;
use JSONAPI\Exception\Http\BadRequest;
use JSONAPI\Exception\Http\NotFound;
use JSONAPI\Exception\Metadata\MetadataException;
use JSONAPI\Exception\Metadata\MetadataNotFound;
use JSONAPI\Exception\Metadata\RelationNotFound;
use JSONAPI\Metadata\ClassMetadata;
use JSONAPI\Metadata\MetadataRepository;
use JSONAPI\Metadata\Relationship;
use JSONAPI\Schema\Resource;
use JSONAPI\Uri\Filtering\Builder\DoctrineQueryExpressionBuilder;
use JSONAPI\Uri\Filtering\ExpressionFilterParser;
use JSONAPI\Uri\LinkFactory;
use JSONAPI\Uri\Pagination\LimitOffsetPagination;
use JSONAPI\Uri\UriParser;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Slim\Http\Response;

/**
 * Class DefaultResourceController
 *
 * @package Controller
 */
class DefaultResourceController implements ResourceController
{

    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;

    /**
     * @var MetadataRepository
     */
    private MetadataRepository $metadataRepository;

    /**
     * @var DefaultRepository|EntityRepository
     */
    private EntityRepository $repository;

    /**
     * @var ClassMetadata
     */
    private ClassMetadata $metadata;
    /**
     * @var DocumentBuilderFactory
     */
    private DocumentBuilderFactory $dbf;
    /**
     * @var DocumentBuilder
     */
    private DocumentBuilder $db;
    /**
     * @var UriParser
     */
    private UriParser $up;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * DefaultResourceController constructor.
     *
     * @param EntityManager          $entityManager
     * @param MetadataRepository     $metadataRepository
     * @param DocumentBuilderFactory $documentBuilderFactory
     * @param LoggerInterface        $logger
     */
    public function __construct(
        EntityManager $entityManager,
        MetadataRepository $metadataRepository,
        DocumentBuilderFactory $documentBuilderFactory,
        LoggerInterface $logger
    ) {
        $this->entityManager      = $entityManager;
        $this->metadataRepository = $metadataRepository;
        $this->dbf                = $documentBuilderFactory;
        $this->logger             = $logger;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @throws BadRequest
     * @throws MetadataNotFound
     */
    private function setupBefore(ServerRequestInterface $request)
    {
        $this->db         = $this->dbf->new($request);
        $this->up         = $this->dbf->uri($request);
        $this->metadata   = $this->metadataRepository->getByType($this->up->getPath()->getResourceType());
        $this->repository = $this->entityManager->getRepository($this->metadata->getClassName());
    }

    /**
     * @param ServerRequestInterface $request
     * @param Response               $response
     *
     * @return ResponseInterface
     * @throws BadRequest
     * @throws DocumentException
     * @throws DriverException
     * @throws MetadataException
     * @throws MetadataNotFound
     */
    public function getCollection(ServerRequestInterface $request, Response $response): ResponseInterface
    {
        $this->setupBefore($request);
        $filter = $this->up->getFilter();
        /** @var LimitOffsetPagination $pagination */
        $pagination = $this->up->getPagination();
        $filter     = new Criteria($filter->getCondition() ?? null, $this->up->getSort()->getOrder());
        $total      = $this->repository->matching($filter)->count();
        $filter->setFirstResult($pagination->getOffset());
        $filter->setMaxResults($pagination->getLimit());
        $data     = $this->repository->matching($filter);
        $document = $this->db->setTotal($total)->setData($data)->build();
        return $response->withJson($document, StatusCodeInterface::STATUS_OK);
    }

    /**
     * @param ServerRequestInterface $request
     * @param Response               $response
     * @param array                  $args
     *
     * @return ResponseInterface
     * @throws BadRequest
     * @throws DocumentException
     * @throws DriverException
     * @throws MetadataException
     */
    public function getResource(ServerRequestInterface $request, Response $response, array $args): ResponseInterface
    {
        $id = $args['id'];
        $this->setupBefore($request);
        if ($entity = $this->repository->find($id)) {
            return $response->withJson($this->db->setData($entity)->build());
        } else {
            throw new NotFound($this->metadata->getType(), $id);
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @param Response               $response
     *
     * @return ResponseInterface
     * @throws BadRequest
     * @throws DocumentException
     * @throws DriverException
     * @throws MetadataException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function createResource(ServerRequestInterface $request, Response $response): ResponseInterface
    {
        $this->setupBefore($request);

        /** @var Document $document */
        $document = $request->getParsedBody();
        /** @var ResourceObject $resource */
        $resource  = $document->getData();
        $className = $this->repository->getClassName();
        $entity    = $this->create($className, $resource);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        $doc = $this->db->setData($entity)->build();
        return $response
            ->withHeader("Location", ($doc->getLinks()[LinkFactory::SELF])->getData())
            ->withJson($doc, StatusCodeInterface::STATUS_CREATED);
    }

    /**
     * @param ServerRequestInterface $request
     * @param Response               $response
     *
     * @return ResponseInterface
     * @throws BadRequest
     * @throws DocumentException
     * @throws DriverException
     * @throws MetadataException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function updateResource(ServerRequestInterface $request, Response $response): ResponseInterface
    {
        $this->setupBefore($request);

        /** @var Document $document */
        $document = $request->getParsedBody();
        /** @var ResourceObject $resource */
        $resource = $document->getData();
        /** @var Entity $entity */
        $entity = $this->repository->find($resource->getId());
        $this->update($entity, $resource);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        $entity = $this->repository->find($resource->getId());
        return $response->withJson($this->db->setData($entity)->build());
    }

    /**
     * @param ServerRequestInterface $request
     * @param Response               $response
     * @param array                  $args
     *
     * @return ResponseInterface
     * @throws BadRequest
     * @throws MetadataNotFound
     * @throws NotFound
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function removeResource(ServerRequestInterface $request, Response $response, array $args): ResponseInterface
    {
        $this->setupBefore($request);

        $id = $args['id'];
        /** @var Entity $entity */
        if ($entity = $this->repository->find($id)) {
            $this->entityManager->remove($entity);
            $this->entityManager->flush();
            return $response->withStatus(StatusCodeInterface::STATUS_NO_CONTENT);
        } else {
            throw new NotFound($this->metadata->getType(), $id);
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @param Response               $response
     * @param array                  $args
     *
     * @return ResponseInterface
     * @throws BadRequest
     * @throws DocumentException
     * @throws DriverException
     * @throws MetadataException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     * @throws RelationNotFound
     */
    public function updateRelationships(
        ServerRequestInterface $request,
        Response $response,
        array $args
    ): ResponseInterface {
        $this->setupBefore($request);

        $id           = $args['id'];
        $name         = $args['name'];
        $entity       = $this->repository->find($id);
        $relationship = $this->metadata->getRelationship($name);
        /** @var Document $document */
        $document = $request->getParsedBody();
        if ($relationship->isCollection) {
            /** @var ResourceCollection $collection */
            $collection = $document->getData();
            $data       = $this->updateRelationshipCollection($relationship, $collection->jsonSerialize());
        } elseif (!is_null($document->getData())) {
            /** @var ResourceObjectIdentifier $rid */
            $rid  = $document->getData();
            $data = $this->entityManager->find($relationship->target, $rid->getId());
        } else {
            $data = null;
        }

        if ($relationship->property) {
            $entity->{$relationship->property} = $data;
        } elseif ($relationship->setter) {
            call_user_func([$entity, $relationship->setter], $data);
        }

        $this->entityManager->flush();

        return $this->getRelationships($request, $response, $args);
    }

    /**
     * @param ServerRequestInterface $request
     * @param Response               $response
     * @param array                  $args
     *
     * @return ResponseInterface
     * @throws BadRequest
     * @throws DocumentException
     * @throws DriverException
     * @throws MetadataException
     * @throws MetadataNotFound
     * @throws NotFound
     * @throws RelationNotFound
     */
    public function getRelationships(
        ServerRequestInterface $request,
        Response $response,
        array $args
    ): ResponseInterface {
        $this->setupBefore($request);
        $id           = $args['id'];
        $name         = $args['name'];
        $relationship = $this->metadata->getRelationship($name);
        /** @var LimitOffsetPagination $pagination */
        $pagination = $this->up->getPagination();
        $filter     = new Criteria($this->up->getFilter()->getCondition() ?? null, $this->up->getSort()->getOrder());
        $entity     = $this->repository->find($id);
        if ($entity) {
            if ($relationship->isCollection) {
                /** @var ArrayCollection|PersistentCollection $data */
                if ($relationship->getter) {
                    $data = call_user_func([$entity, $relationship->getter]);
                } else {
                    $data = $entity->{$relationship->property};
                }
                $total = $data->matching($filter)->count();
                $filter->setFirstResult($pagination->getOffset());
                $filter->setMaxResults($pagination->getLimit());
                $data     = $data->matching($filter)->toArray();
                $document = $this->db->setTotal($total)->setData($data)->build();
            } else {
                if ($relationship->getter) {
                    $data = call_user_func([$entity, $relationship->getter]);
                } else {
                    $data = $entity->{$relationship->property};
                }
                $document = $this->db->setData($data)->build();
            }
            return $response->withJson($document);
        } else {
            throw new NotFound($this->metadata->getType(), $id);
        }
    }

    /**
     * Only for toMany
     *
     * @param ServerRequestInterface $request
     * @param Response               $response
     * @param array                  $args
     *
     * @return ResponseInterface
     * @throws BadRequest
     * @throws DocumentException
     * @throws DriverException
     * @throws MetadataException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws RelationNotFound
     * @throws TransactionRequiredException
     */
    public function createRelationships(
        ServerRequestInterface $request,
        Response $response,
        array $args
    ): ResponseInterface {
        $this->setupBefore($request);
        $id   = $args['id'];
        $name = $args['name'];
        if ($entity = $this->repository->find($id)) {
            $relation = $this->metadata->getRelationship($name);
            /** @var Document $document */
            $document = $request->getParsedBody();
            /** @var ResourceCollection $resourceObjects */
            $resourceObjects = $document->getData();
            if (($relation->property || $relation->setter) && is_array($resourceObjects)) {
                /** @var Collection $data */
                if ($relation->property) {
                    $data = $entity->{$relation->property};
                } else {
                    $data = call_user_func([$entity, $relation->getter]);
                }
                $data = new ArrayCollection($data->toArray());
                foreach ($resourceObjects as $relationship) {
                    $rel = $this->entityManager->find($relation->target, $relationship->getId());
                    if (!$data->contains($rel)) {
                        $data->add($rel);
                    }
                }

                if ($relation->property) {
                    $entity->{$relation->property} = $data;
                } elseif ($relation->setter) {
                    call_user_func([$entity, $relation->setter], $data);
                }
                $this->entityManager->flush();
            } else {
                throw new BadRequest("Relationship must be writable and data must be collection");
            }
            return $this->getRelationships($request, $response, $args);
        } else {
            throw new NotFound($this->metadata->getType(), $id);
        }
    }

    /**
     * Only for toMany
     *
     * @param ServerRequestInterface $request
     * @param Response               $response
     * @param array                  $args
     *
     * @return ResponseInterface
     * @throws BadRequest
     * @throws DocumentException
     * @throws DriverException
     * @throws MetadataException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws RelationNotFound
     * @throws TransactionRequiredException
     */
    public function removeRelationships(
        ServerRequestInterface $request,
        Response $response,
        array $args
    ): ResponseInterface {
        $this->setupBefore($request);

        $id       = $args['id'];
        $name     = $args['name'];
        $entity   = $this->repository->find($id);
        $relation = $this->metadata->getRelationship($name);
        /** @var Document $document */
        $document = $request->getParsedBody();
        /** @var ResourceCollection $resourceObjects */
        $resourceObjects = $document->getData();
        if (($relation->property || $relation->setter)) {
            /** @var Collection $data */
            if ($relation->property) {
                $data = $entity->{$relation->property};
            } else {
                $data = call_user_func([$entity, $relation->getter]);
            }
            $data = new ArrayCollection($data->toArray());
            foreach ($resourceObjects as $relationship) {
                $rel = $this->entityManager->find($relation->target, $relationship->getId());
                if ($data->contains($rel)) {
                    $data->removeElement($rel); //todo: this not work
                }
            }

            if ($relation->property) {
                $entity->{$relation->property} = $data;
            } elseif ($relation->setter) {
                call_user_func([$entity, $relation->setter], $data);
            }
            $this->entityManager->flush();
        }
        return $this->getRelationships($request, $response, $args);
    }

    /**
     * @param string         $className
     * @param ResourceObject $resource
     *
     * @return mixed
     * @throws Exception
     */
    protected function create(string $className, ResourceObject $resource): Entity
    {
        $entity = new $className();
        return $this->update($entity, $resource);
    }

    /**
     * @param Entity         $entity
     * @param ResourceObject $resource
     *
     * @return Entity
     * @throws Exception
     */
    protected function update(Entity $entity, ResourceObject $resource): Entity
    {
        $metadata = $this->metadataRepository->getByClass(get_class($entity));
        $this->setAttributes($entity, $resource, $metadata);
        $this->setRelationships($entity, $resource, $metadata);
        return $entity;
    }

    /**
     * @param Entity         $entity
     * @param ResourceObject $resource
     * @param ClassMetadata  $metadata
     *
     * @throws Exception
     */
    protected function setAttributes(Entity $entity, ResourceObject $resource, ClassMetadata $metadata)
    {
        foreach ($metadata->getAttributes() as $attribute) {
            try {
                $value = $resource->getAttribute($attribute->name);
                if ($attribute->property) {
                    $entity->{$attribute->property} = $value;
                } elseif ($attribute->setter) {
                    if ($attribute->type === DateTimeInterface::class) {
                        call_user_func([$entity, $attribute->setter], new DateTime($value));
                    } else {
                        call_user_func([$entity, $attribute->setter], $value);
                    }
                }
            } catch (FieldNotExist $notSet) {
                // NO-SONAR
            }
        }
    }

    /**
     * @param Entity         $entity
     * @param ResourceObject $resource
     * @param ClassMetadata  $metadata
     */
    protected function setRelationships(Entity $entity, ResourceObject $resource, ClassMetadata $metadata): void
    {
        foreach ($metadata->getRelationships() as $relationship) {
            try {
                $value = $resource->getRelationship($relationship->name);
                if ($relationship->isCollection) {
                    /** @var Collection $collection */
                    if ($relationship->property) {
                        $collection                        = $this->updateRelationshipCollection($relationship, $value);
                        $entity->{$relationship->property} = $collection;
                    } elseif ($relationship->setter) {
                        $collection = $this->updateRelationshipCollection($relationship, $value);
                        call_user_func([$entity, $relationship->setter], $collection);
                    }
                } else {
                    $relation = $this->entityManager->getRepository($relationship->target)->find($value->getId());
                    if ($relationship->property) {
                        $entity->{$relationship->property} = $relation;
                    } elseif ($relationship->setter) {
                        call_user_func([$entity, $relationship->setter], $relation);
                    }
                }
            } catch (FieldNotExist $notSet) {
                // NO-SONAR
            }
        }
    }

    /**
     * @param Relationship               $relationship
     * @param ResourceObjectIdentifier[] $value
     *
     * @return Collection
     * @throws EntityNotFoundException
     */
    protected function updateRelationshipCollection(
        Relationship $relationship,
        array $value
    ): Collection {
        $collection = new ArrayCollection();
        foreach ($value as $item) {
            if ($relation = $this->entityManager->getRepository($relationship->target)->find($item->getId())) {
                $collection->add($relation);
            } else {
                throw new EntityNotFoundException('Entity ' . $relationship->target
                    . ' with ID: ' . $item->getId() . ' not found.');
            }

        }
        return $collection;
    }
}
