<?php

use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\Cache;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Processor\MemoryPeakUsageProcessor;
use Monolog\Processor\MemoryUsageProcessor;
use Monolog\Processor\PsrLogMessageProcessor;
use Psr\Log\LoggerInterface;

use function DI\autowire;

return [
    'displayErrorDetails' => true,
    'isDev' => 2,
    Cache::class => autowire(ArrayCache::class),
    LoggerInterface::class => function () {
        return new Logger(
            'API',
            [
                new StreamHandler('php://stdout', Logger::DEBUG)
            ],
            [
                new PsrLogMessageProcessor(),
                new IntrospectionProcessor(Logger::ERROR),
                new MemoryPeakUsageProcessor(),
                new MemoryUsageProcessor(false)
            ]
        );
    }
];
