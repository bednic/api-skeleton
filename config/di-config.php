<?php

use API\Controller\DefaultResourceController;
use API\Controller\ResourceController;
use API\Factory\ResourceRouteRegistrar;
use API\Middleware\CORSMiddleware;
use API\Service\Logging\CompositeSQLLogger;
use Doctrine\Common\Cache\ApcuCache;
use Doctrine\Common\Cache\Cache;
use Doctrine\Common\EventManager;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Driver as DoctrineDriver;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Logging\SQLLogger;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use JSONAPI\DocumentBuilderFactory;
use JSONAPI\Driver\AnnotationDriver;
use JSONAPI\Driver\Driver as JsonApiDriver;
use JSONAPI\Metadata\MetadataFactory;
use JSONAPI\Metadata\MetadataRepository;
use JSONAPI\Middleware\PsrJsonApiMiddleware;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Processor\PsrLogMessageProcessor;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use Roave\DoctrineSimpleCache\SimpleCacheAdapter;
use Slim\App;
use Slim\CallableResolver;
use Slim\Factory\AppFactory;
use Slim\Http\Factory\DecoratedResponseFactory;
use Slim\Interfaces\CallableResolverInterface;
use Slim\Interfaces\RouteCollectorInterface;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Routing\RouteCollector;
use function DI\autowire;
use function DI\env;
use function DI\factory;
use function DI\get;

return [
    'env' => env('API_ENV', 'prod'),
    'baseURL' => env('API_URL', 'http://localhost'),
    'displayErrorDetails' => false,
    'logErrors' => true,
    'logErrorDetails' => true,
    'isDev' => false,
    'db' => [
        'host' => env('DB_HOST', 'localhost'),
        'port' => env('DB_PORT', 5432),
        'dbname' => env('DB_NAME', 'app'),
        'user' => env('DB_USER', 'app'),
        'password' => env('DB_PASSWORD', 'secret')
    ],
    App::class => factory([AppFactory::class, 'createFromContainer'])
        ->parameter('container', get(ContainerInterface::class)),

    CallableResolverInterface::class => autowire(CallableResolver::class),
    RouteCollectorInterface::class => autowire(RouteCollector::class)
        ->constructorParameter('container', get(ContainerInterface::class)),
    CORSMiddleware::class => autowire(),
    StreamFactoryInterface::class => autowire(StreamFactory::class),
    ResponseFactoryInterface::class => function (ContainerInterface $c) {
        return new DecoratedResponseFactory(new ResponseFactory(), $c->get(StreamFactoryInterface::class));
    },

    Cache::class => autowire(ApcuCache::class),
    CacheInterface::class => autowire(SimpleCacheAdapter::class),

    LoggerInterface::class => function () {
        return new Logger(
            'API',
            [
                new RotatingFileHandler(LOG . '/error.log', 7, Logger::ERROR),
            ],
            [
                new PsrLogMessageProcessor(),
                new IntrospectionProcessor(Logger::ERROR)
            ]
        );
    },
    SQLLogger::class => autowire(CompositeSQLLogger::class),

    DoctrineDriver::class => autowire(DoctrineDriver\PDO\MySQL\Driver::class),
    EventManager::class => autowire(),
    Configuration::class => factory([Setup::class, 'createAnnotationMetadataConfiguration'])
        ->parameter('paths', [__DIR__ . '/../src/Model/Entity'])
        ->parameter('isDevMode', get('isDev'))
        ->parameter('proxyDir', CACHE . '/_Doctrine.Proxy')
        ->parameter('cache', get(Cache::class))
        ->parameter('useSimpleAnnotationReader', false)
        ->parameter('logger', get(SQLLogger::class)),
    Connection::class => autowire(\Doctrine\DBAL\Connection::class)
        ->constructorParameter('params', get('db'))
        ->constructorParameter('driver', get(DoctrineDriver::class))
        ->constructorParameter('config', get(Configuration::class))
        ->constructorParameter('eventManager', get(EventManager::class)),
    EntityManager::class => factory([EntityManager::class, 'create'])
        ->parameter('connection', get(Connection::class))
        ->parameter('config', get(Configuration::class))
        ->parameter('eventManager', get(EventManager::class)),

    ResourceController::class => autowire(DefaultResourceController::class),
    ResourceRouteRegistrar::class => autowire()
        ->constructorParameter('baseURL', get('baseURL')),

    JsonApiDriver::class => autowire(AnnotationDriver::class),
    MetadataRepository::class => factory([MetadataFactory::class, 'create'])
        ->parameter('paths', [__DIR__ . '/../src/Model/Entity'])
        ->parameter('cache', get(CacheInterface::class))
        ->parameter('driver', get(JsonApiDriver::class))
        ->parameter('logger', get(LoggerInterface::class)),
    DocumentBuilderFactory::class => autowire()
        ->constructorParameter('baseURL', get('baseURL')),
    PsrJsonApiMiddleware::class => autowire()
        ->constructorParameter('baseURL', get('baseURL'))
        ->constructorParameter('logger', get(LoggerInterface::class))
];
